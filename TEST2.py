# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 09:50:45 2019

@author: Measurement
"""
import atexit
import numpy as np
import pyqtgraph as pg
import modules.devicecontrol as dc
class table:
    def __init__(self):
        self.slope = 0.0
        self.intercept = 0.0

class Script():
    def __init__(self):
        super(Script, self).__init__()
        self.daq = dc.DAQ()
        self.dac = dc.DAC()
        self.data = {'current':[], 'voltage':[]}

    def work(self, start, fin, steps, chan, resistance):
        self.set_current = np.zeros(steps+1)
        for i in range(steps+1):
            self.set_current[i]=start + (fin-start)/(steps+1)*i
            Vdac = self._current(self.set_current[i]) 
            self.dac.AOut(chan, Vdac)
            time.sleep(0.1)
            voltage = self.daq.read_linux(chan)[0]#/resistance
            self.data['current'].append(self.set_current[i])
            self.data['voltage'].append(voltage)
        pg.plot(self.data['current'], self.data['voltage'])
        
    def _current(self):
        Va = (current * 37.8)/1000
        #set voltage on DAQ, which is Va divided by the value of voltage divider
        Vdac = Va / 0.163
        return Vdac
  @atexit.register
  def on_exit(self):
    self.daq.udev.close()
    self.dac.h.close()
    print("Script closed")
