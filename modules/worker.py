# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 09:48:19 2019

@author: Measurement
"""
import logging
import os
import csv
import threading
import time
from PyQt5.QtCore import QObject, QTimer, pyqtSlot, pyqtSignal
import openpyxl
import modules.sendmail as sendmail
import random
def logthread(caller):
        print('%-25s: %s, %s,' % (caller, threading.current_thread().name,
                                  threading.current_thread().ident))
class WorkerObject(QObject):
    """worker class for handling measurements stuff"""
    updateLabel = pyqtSignal(int, float)
    updatePushButton = pyqtSignal(int, str, bool)
    start = pyqtSignal()
    finished = pyqtSignal()
    sendToServer = pyqtSignal(str)
    update_parameters = pyqtSignal(int, str, str, float, int, bool)
    update_parameters_cycle = pyqtSignal(int, str, str, float, int, bool, int)
    init_measurements = pyqtSignal(int, bool)
    update_counter_label = pyqtSignal(int, int, int, str)
    set_AOut = pyqtSignal(int, float)
    def __init__(self, daq, dac):
        super(WorkerObject, self).__init__()
        self.daq = daq
        self.dac = dac
        self.init_parameters()
        self.init_dicts()
        self.init_measurements.connect(self._init_measurements)
        self.update_parameters.connect(self.update_measurement_parameters)
        self.update_parameters_cycle.connect(self.update_measurement_parameters)
        self.start.connect(self.run)
    def init_parameters(self):
        self.relax_time = 45
        self.channel_number = 0
        self.warning_charge = 4.2
        self.warning_discharge = 3.0
        self.started = False
        self.send_email = False
        self.addressList = []
        self.delay = 2
    @pyqtSlot()
    def run(self):
        """create QTimer for trigger loop and start it"""
        # logthread('WorkerObject / run  ')
        if not self.started:
            # logthread('WorkerObject / run not started ')                                    #
            self.started = True
            self.loop_timer = QTimer(self)
            self.loop_timer.timeout.connect(self.measurement_loop)
            self.loop_timer.start(self.delay*1000) #get measurements every 2 seconds
    def _init_measurements(self, channel, auto_trigger):
        """ Initialize measurement: update excel log file, create new .csv to store data,
            set DAC and DAQ, finally turn on channel status to start reading data
        """
        # logthread('_init_measurements')
        print("Initializing new operation")
        self.excel_logfile_update(channel)
        self.csv_file_create(channel)

        self.daq.DOut(channel, self.meas_type[channel])
        self.set_AOut.emit(channel, self.current[channel])


        self.updatePushButton.emit(channel, 'STOP', True)
        self.clear_data_dictionary(channel)
        self.channel_status[channel] = 'ON'
        if auto_trigger:
            self.start.emit() # in case all measurements are off
        print("Operation started! Channel {}".format(channel))

    def measurement_loop(self):
        """main loop where measurements are gathered and processed"""
        for channel in [ch for ch, status in self.channel_status.items() if status == 'ON']:
            voltage = float(self.daq.read_linux(channel)[0])
            # voltage = self.daq.read(channel)
            #round time to 2 decimal places:
            read_time = int((time.time() - self.start_time[channel])*(10**2))/(10.**2)
            # voltage = random.random()+4
            # print("measurement_loop: reading gathered")
            self.update_single_measurement_dictionary(channel, voltage, read_time)
            self.update_dictionary_with_measurements(channel, self.single_measurement_dictionary[channel])
            self.updateLabel.emit(channel, voltage)
            self.check_finish_condition(channel, voltage, self.meas_type[channel])
        if not 'ON' in self.channel_status.values():
            self.started = False
            self.loop_timer.stop()
    def check_finish_condition(self, channel, read_value, meas_type):
        """ Set DAC D/A channel to 0 V when charge/discharge finishes
            Check if battery is set to cycle or single-operation mode
            Send and email with results if checked"""
        if meas_type == 'charge':
            warning_voltage = self.warning_charge
            if read_value > warning_voltage:
                if not self.cycle[channel]:
                    self.stop_measurement(channel, True)
                    print(('Channel: {}\n '
                           'Voltage over {} - '
                           'measurement stopped'.format(channel, warning_voltage)))
                else:
                    self.stop_measurement(channel, False)
                    self.init_next_cycle_operation(channel)
                    # print("into init_next_cycle_operation")
                if self.send_email:
                    self.send_mail(channel, warning_voltage)
        else:
            warning_voltage = self.warning_discharge
            if read_value < warning_voltage:
                if not self.cycle[channel]:
                    self.stop_measurement(channel, True)
                    print(('Channel number - {}:'
                           'Voltage under {} - '
                           'measurement stopped'.format(channel, warning_voltage)))
                else:
                    self.stop_measurement(channel, False)
                    self.init_next_cycle_operation(channel)
                if self.send_email:
                    self.send_mail(channel_number, warning_voltage)
    def init_next_cycle_operation(self, channel):
        """ Prepare all measurement data before starting new operation.
            Update information about:
                - operations finished
                - next measurement type
                - new date of measurement start
                - new operation number
                - new file name
                - label with cycle counter
            Clear previous list with data
            Trigger timer which starts measurements
        """
        # print('init_next_cycle_operation: CHANNEL {}'.format(channel))
        if self.cycle_operations_finished[channel] == self.cycle_num[channel]*2:
            print(('Battery cycling has finished. '
                   'Total number of operations: {}. '
                   'Channel: {}'.format(self.cycle_operations_finished[channel], channel)))
            self.updatePushButton.emit(channel, 'START', True)
        else:
            print("Initialize next cycle operation...")
            self.cycle_operations_finished[channel] += 1
            #update all informations about new measurement:
            if self.meas_type[channel] == 'charge':
                print("Charge finished, initializing discharge... Channel: {}".format(channel))
                self.meas_type[channel] = 'discharge'
            else:
                print("Discharge finished, initializing charge... Channel: {}".format(channel))
                self.meas_type[channel] = 'charge'

            date = time.strftime("%Y-%m-%d--%H-%M-%S")
            self.date_channel[channel] = date
            
            self.start_time[channel] = time.time()

            # get last operation number from last filename:
            last_operation_number = int(self.filename[channel].split(os.sep)[-1].split('_')[-1][:-4])
            self.operation_number[channel] = last_operation_number + 1
            self.filename[channel] = self.set_file_name(channel)

            op_start_time = time.strftime('%H:%M:%S',time.localtime(time.time()+self.relax_time))
            print(("Measurement data updated, battery is ready and next operation "
                   "will start at {}. Channel: {}".format(op_start_time, channel)))
            #flush list with data
            self.clear_data_dictionary(channel)
            self.update_counter_label.emit(channel, self.cycle_operations_finished[channel], self.cycle_num[channel]*2, self.meas_type[channel])
            QTimer.singleShot(self.relax_time*1000, lambda: self._init_measurements(channel, True))

    def update_measurement_parameters(self, channel, sampleID, meas_type, current, operation_number, cycle, *cycle_num):
        """ Update worker thread with data acquired from GUI.
            Prepare all measurement data required to create log file and to update excel file"""
        self.sampleID[channel] = sampleID
        # print("update_measurement_parameters: sampleID channel updated")

        self.date_channel[channel] = time.strftime("%Y-%m-%d--%H-%M-%S")
        # print("update_measurement_parameters: date channel updated")
        
        self.meas_type[channel] = meas_type
        # print("update_measurement_parameters: meas_type channel updated")

        self.current[channel] = current
        # print("update_measurement_parameters: current channel updated")

        self.operation_number[channel] = operation_number
        # print("update_measurement_parameters: operation_number channel updated")

        self.cycle[channel] = cycle
        # print("update_measurement_parameters: cycle channel updated")

        self.start_time[channel] = time.time()
        # print("update_measurement_parameters: start_time channel updated")
        
        if cycle_num:
            self.cycle_num[channel] = cycle_num[0]
            # print("update_measurement_parameters: cycle_num channel updated")
            self.cycle_operations_finished[channel] = 1
            # print("update_measurement_parameters: cycle_operations_finished channel updated")

        self.filename[channel] = self.set_file_name(channel)
        # print("update_measurement_parameters: filename channel updated")
    def clear_data_dictionary(self, channel):
        self.data_dictionary[channel]['Voltage'].clear()
        self.data_dictionary[channel]['time'].clear()
        self.data_dictionary[channel]['Capacity'].clear()

    def set_file_name(self, channel):
        """ Set full directory to the measurement log file.
        
        if sampleID is provided by lineEdit box, check if folder with sampleID exists.
            If it does - return full path with existing folder, where log file will be created.
            If it doesn't - create new folder with sampleID name
        if sampleID isn't provided, the log file is created in main MEASUREMENTS folder
        
        Returns:
            string of full directory to the measurement log file
        """
        sampleID = self.sampleID[channel]
        date = self.date_channel[channel]
        meas_type = self.meas_type[channel]
        operation_number = self.operation_number[channel]

        if sampleID:
            if not os.path.isdir(self.directory+'{}'.format(sampleID)):
                os.mkdir(self.directory+'{}'.format(sampleID))
            directory = self.directory+'{}{}'.format(sampleID,os.sep)
        else:
            directory = self.directory
        if not operation_number: #either it's empty or 0 we don't add it to filename
            filename = (directory
                        + '{0}_{1}_{2}.csv'.format(date, sampleID, meas_type))
        else:
            filename = (directory
                        + '{0}_{1}_{2}_{3}.csv'.format(date, sampleID,
                                                       meas_type,
                                                       operation_number))
        if os.path.isfile(filename + '.csv'):
            i = 1
            filename_temp = filename
            while os.path.isfile(filename_temp + '({})'.format(i) + '.csv'):
                i = i + 1
            filename = filename_temp + '({})'.format(i)
        return filename
    def excel_logfile_update(self, channel):
        """Append measurements excel log file, directory of excel file is hard coded
        Args:

        channel: int
            Channel number of which we do measurements
        full_path: str
            full directory path to log file
        date: str
            timestamp of measurement start
        sampleID: str
        operation_number: int
        meas_type: str
            type of measurement. charge od discharge
        """
        full_path = self.filename[channel]
        date = self.date_channel[channel]
        sampleID = self.sampleID[channel]
        operation_number = self.operation_number[channel]
        meas_type = self.meas_type[channel]
        current = self.current[channel]

        # name of the last folder in directory:
        dir_lastfolder = os.path.basename(os.path.dirname(full_path))
        # name of last file in folder, we have to remove it from full path
        file_name = os.path.basename(os.path.normpath(full_path))
        if dir_lastfolder == 'Measurements':
            dir_to_print = "/Measurements"
        else:
            #if our .csv log is in, for example: C:\\Folder1\\TestSample\\abc.csv
            #in Excel log file, under location we'll see Folder1\\TestSample
            sampleID_folder = os.path.dirname(full_path).split(os.sep)[-1]
            base_folder = os.path.dirname(full_path).split(os.sep)[-2]
            dir_to_print = base_folder + os.sep + sampleID_folder
        hyperlink = '=HYPERLINK("{}", "{}")'.format(os.path.dirname(full_path),
                                                    dir_to_print
                                                   )
        try:
            if self.operating_system == 'Windows':
                worksheet_path = ("C:\\Users\\Measurement\\Desktop\\Measurements\\"
                                "Measurements_LogFile.xlsx")
            else:
                worksheet_path = "/home/pi/Documents/Measurements/Measurements_LogFile.xlsx"
            date = date[0:10].replace('-', '.')+' '+date[12:20].replace('-', ':')
            book = openpyxl.load_workbook(worksheet_path)
            sheet = book.active
            sheet.append([sampleID, operation_number,
                          'Board//Channel {}'.format(channel), date, meas_type,
                          current, file_name, hyperlink]
                        )
            book.save(worksheet_path)
            print("Excel file log updated successfully")
        except IOError:
            logging.warning('excel file was not available')
            print(("CAN'T APPEND CURRENT MEASUREMENT TO LOG FILE - "
                   "PROBABLY IT'S OPENED IN EXCEL"))

    def update_single_measurement_dictionary(self, channel, voltage_value, read_time):
        self.single_measurement_dictionary[channel]['Voltage'] = voltage_value
        self.single_measurement_dictionary[channel]['time'] = read_time
        self.single_measurement_dictionary[channel]['Capacity'] = read_time*self.current[channel]/3600
    def update_dictionary_with_measurements(self, channel, measurement_dict):
        """dictionary with all data which is used to plot"""
        for key in self.data_dictionary[channel].keys():
            self.data_dictionary[channel][key].append(measurement_dict[key])
        self.csv_file_append(measurement_dict, channel)
    def csv_file_create(self, channel):
        """create new .csv file with headers"""
        directory = self.filename[channel]
        fieldnames = ['Voltage', 'time', 'Capacity']
        with open(directory, 'w', newline='') as file:
            writer = csv.DictWriter(file, fieldnames=fieldnames, delimiter='\t')
            writer.writeheader()
        # print("csv log file created")
    def csv_file_append(self, dict_to_save, channel):
        """append file with single measurement"""
        fieldnames = ['Voltage', 'time', 'Capacity']
        with open(self.filename[channel], 'a', newline='') as file:
            writer = csv.DictWriter(file, fieldnames=fieldnames, delimiter='\t')
            writer.writerow(dict_to_save)
    def stop_measurement(self, channel, enabled):
        """ To stop measurement, set D/A channel to 0 V
            Update channel status to OFF,
            Update PushButton status
            Send measurement log to file
        """
        # set D/A output to 0 V (DAC):
        self.dac.AOut(channel, 0)

        self.channel_status[channel] = 'OFF'
        self.updatePushButton.emit(channel, 'START', enabled)
        self.sendToServer.emit(self.filename[channel])
        print("measurement stopped")
    def send_mail(self, channel, warning_voltage):
        """create a new thread to not freeze the reading from device
        and send an email"""
        thread_mail = threading.Thread(target=sendmail.sendMail,
                                       args=(channel, warning_voltage,
                                             self.filename[channel],
                                             self.addressList
                                             ))
        thread_mail.start()
    def init_dicts(self):
        """Initialize all dictionaries which are used to handle measurements on all channels"""
        self.channel_status = {i:'OFF' for i in range(8)} # status: ON or OFF
        self.meas_type = {}  # 'charge' or 'discharge'
        self.date_channel = {}
        self.sampleID = {}
        self.current = {}
        self.filename = {}
        self.start_time = {}
        self.data_dictionary = {i: {'Voltage': [], 'time': [], 'Capacity': []} for i in range(8)}
        self.cycle = {}
        self.cycle_num = {}
        self.cycle_operations_finished = {}
        self.operation_number = {}
        self.single_measurement_dictionary = {i: dict.fromkeys(['Voltage',
                                                                'time',
                                                                'Capacity'])
                                              for i in range(8)
                                             }
