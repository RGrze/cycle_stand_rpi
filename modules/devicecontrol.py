# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 09:50:45 2019

@author: Measurement
"""
from PyQt5.QtCore import QObject, pyqtSignal
import hid
import libusb1
import usb1
from struct import *

class table:
    def __init__(self):
        self.slope = 0.0
        self.intercept = 0.0


class DAQ(QObject):
    """Class to control DAQ USB-2408"""
    def __init__(self):
        super(DAQ, self).__init__()
        self.init_board_linux()
        self.DIO_channel=[0,0,0,0,0,0,0,0]

    def init_board_linux(self):
        #Range:
        self.BP_10_00V = 0x1 #+/- 10V
        #Mode:
        self.DIFFERENTIAL = 0x0
        self.SINGLE_ENDED = 0xb
        self.CONTINOUS = 0x1

        self.DIN = 0x00 #read digital port value
        self.DOUT = 0x01
        self.AIN = 0x10 # read analog input channel

        self.productID = 0x00fd
        self.udev = self.openByVendorIDAndProductID(0x9db, self.productID, serial=None)
        if not self.udev:
            raise IOError("MCC USB-2408 not found")
            return

    def openByVendorIDAndProductID(self, vendor_id, product_id, serial):
        self.context = usb1.USBContext()
        for device in self.context.getDeviceIterator(skip_on_error=False):
          if device.getVendorID() == vendor_id and device.getProductID() == product_id:
            if serial == None:
              return device.open()
            else:
              if device.getSerialNumber() == serial:
                return device.open()
        return None   
    def DOut(self, channel, meas_type, port=0):
        """change DIO channel value

        Args:

        channel: int 
             DIO channel number
        state: int 
            0 or 1
            set 5V output: state = 0;
            set 0V output: state = 1
        """
        if meas_type == 'charge':
            state = 0
        else:
            state = 1
        self.DIO_channel[channel] = state
        value='0b{}{}{}{}{}{}{}{}'.format(self.DIO_channel[7], self.DIO_channel[6], self.DIO_channel[5],
                                          self.DIO_channel[4], self.DIO_channel[3], self.DIO_channel[2],
                                          self.DIO_channel[1], self.DIO_channel[0])
        #Convert binary to decimal value:
        value = int(value,2)
        request_type = libusb1.LIBUSB_TYPE_VENDOR 
        # wValue = 0
        # wIndex = 0
        # result = self.udev.controlWrite(request_type, self.DOUT, wValue, wIndex, [port, value], timeout = 100)
        result = self.udev.controlWrite(request_type, 0x01, 0, 0, [port, value], timeout = 100)
    def read_linux(self, channel, mode=0x0, gain=0x1, rate=5):
        '''
             The command returns the value from the specified analog input channel.  The channel
             configuration is specified in the command.  The command will result in a bus stall
             if an AInScan is currently running.

             channel: 0-15 or 31  the input channel to read
             mode:    0: Differential voltage
                      1: Single-ended voltage, high pin
                      2: Single-ended voltage, low pin
                      3: D/A readback voltage
                      4: Thermocouple
                      5: AIn offset calibration
                      6: AIn gain calibration
                      7: TC Offset Calibration
                      8: TC Gain Calibration Positive
                      9: TC Gain Calibration Negative
                     10: Thermocouple no burnout detect
             range:   0-8  
             rate:    0-15 

             value: signed 24 bit value frad from the analog input channel
             flags: bits  0-6: reserved
                    bit7: 1 = TC open detected,  0 = normal reading
        '''
        wValue = (mode << 8) | channel
        wIndex = (rate << 8) | gain
        request_type = libusb1.LIBUSB_TYPE_VENDOR
        data ,= unpack('I',self.udev.controlRead(request_type, 0x10, wValue, wIndex, 4, timeout = 200))
        flags = (data >> 24)
        data = self.int24ToInt(data)

        volt = self.volts(data)
        return (volt, flags)

    def int24ToInt(self, int24val):
        # convers a 2's complement signed 24 bit number to an int (32 or 64 bit)
        SIGN_BITMASK = (1 << 23)
        FULL_SCALE24_BITMASK = ((1<<24) - 1)
        SIGN_EXT_BITMASK = (~FULL_SCALE24_BITMASK)
        int24val &= 0xffffff
        if int24val & SIGN_BITMASK:
          int24val |= SIGN_EXT_BITMASK
        else:
          int24val &= FULL_SCALE24_BITMASK;
        return int24val
    def volts(self, value, gain=0x1):
        '''
        converts 24 bit signed value to volts
        gain:
          BP_10_00V  = 0x1         # +/- 10.0 V
          BP_5_00V   = 0x2         # +/- 5.00 V
          BP_2_50V   = 0x3         # +/- 2.50 V
          BP_1_25V   = 0x4         # +/- 1.25 V
          BP_0_625V  = 0x5         # +/- 0.625 V
          BP_0_312V  = 0x6         # +/- 0.3125 V
          BP_0_156V  = 0x7         # +/- 0.15625 V
          BP_0_078V  = 0x8         # +/- 0.078125 V (Voltage and Thermocouple)
        '''
        if gain == 0x1:
          volt = value * 10.0 / 0x7fffff;
        elif gain == 0x2:
          volt = value * 5.0 / 0x7fffff;
        elif gain == 0x3:
          volt = value * 2.5 / 0x7fffff;
        elif gain == 0x4:
          volt = value * 1.25 / 0x7fffff;
        elif gain == 0x5:
          volt = value * 0.625 / 0x7fffff;
        elif gain == 0x6:
          volt = value * 0.3125 / 0x7fffff;
        elif gain == 0x7:
          volt = value * 0.15625 / 0x7fffff;
        elif gain == 0x8:
          volt = value * 0.078125 / 0x7fffff;
        elif gain == 9:
          volt = value * 0.078125 / 0x7fffff;
        else:
          raise ValueError('volts: Unknown voltage range.')
          return 
        return volt
class DAC(QObject):
    """Class to control DAC"""
    def __init__(self):
        super(DAC, self).__init__()
        # self.init_board()
        self.CalTable = [table(), table(), table(), table(), table(), table(), table(), table(), \
                         table(), table(), table(), table(), table(), table(), table(), table()]
        self.init_board_linux()

    # RASPBERRY
    def DOut(self, value):
        """
        This command writes data to the DIO port bits that are configured
        as outputs.
          value:  value to write to the port
        """
        self.h.write([0x04, value])
    def AOut(self, channel, Vout, update=0):
        """
        This command writes the value to an analog output channel.  The
        value is a 16-bit unsigned value.  The output range for a channel
        may be set with AOutConfig.  The equation for the output voltage is:

        V_out = (k - 2^15/2^15) * 10V    for +/- 10V range

        V_out = (k/2^16) * 10V           for 0-10V range 

        where k is the value written to the device.  The equation for
        current output is:
        
        I_out = (k/2^16) * 5V/249 Ohm    for 0-20mA range.

        where k is the value written to the device.  The current output
        value is independent of the rnage selection.  A current and
        voltage are always ouptut for a given vluae.

        channel: the channel to write (0-15)
        value:   the value to write
        update:  update mode:  0 = update immediately, 1 = update on sync signal

        command:
            0x14: set analog out
        """

        # value = self.volts(Vout)
        value = self.volts(0,Vout)
        # value = (Vout*(2**16))/10
        value = int(self.CalTable[channel].slope*value + self.CalTable[channel].intercept)
        # print("Value to DAC: {}".format(value))
        self.h.write([0x14, channel, (value & 0xff), (value>>8) & 0xff, update])

    def init_board_linux(self):
        self.AOUT = 0x14
        self.h = hid.device()
        self.productID = 0x009E
        self.h.open(0x09db, self.productID, None)
        self.h.set_nonblocking(1)
        self.DConfigPort(0x0)
        self.DOut(0)
        for i in range(9):
            self.AOutConfig(i)

    def DConfigPort(self, direction):
        """
        This command sets the direction of the DIO port bits to input or output
        direction:  0 = output,  1 = input
        """
        self.h.write([0x01, direction])
    def AOutConfig(self, channel, gain=0):
        """
        This command configures the output range of an analog outout
        channel.  The output will be set to 0V in the selected range, so
        an AOut is not needed after this command.

        channel: the channel to write (0-15)
        gain:    the output gain (don't care when doing current output)
                 0 = 0-10V 1 = +/- 10V
        #Command:
            0x1C: Configure analog output channel
        gain:
            UP_10_00V: 0 0-10V 
            BP_10_00V: 1 +/- 10V
            I_0_20mA: 2 0-20 mA
        """
        self.h.write([0x1C, channel, gain])

        if gain == 0:
          address = 0x100 + 0x10*channel
          # print('DAC AOut range 0-10V')
        elif gain == 1:
          address = 0x108 + 0x10*channel
        elif gain == 2:
          address = 0x200 + 0x8*channel
        else:
          raise ValueError('AOutConfig: gain value out of range')
          return

        self.CalTable[channel].slope ,= unpack('f', self.MemRead(address, 4))
        address += 4
        self.CalTable[channel].intercept ,= unpack('f', self.MemRead(address, 4))
        # print('Channel {}\nslope: {}\nintercept: {}'.format(channel, self.CalTable[channel].slope, self.CalTable[channel].intercept))
        print("AOut coefficients for channel {} loaded".format(channel))
    def MemRead(self, address, count):
        """
        This command reads data from the configuration memory (EEPROM or
        FLASH).  All of the memory may be read.  The EEPROM address are
        from 0x0000 to 0x00FF.  The FLASH addresses are from 0x0100 -
        0x02FF.

        address:  the start address for the read.
        type:     not used
        count:    the number of bytes to read (62 max)

        command:
            0x30: read memory
        """

        if (count > 62):
            raise ValueError('MemRead: max count is 62.')
            return
        self.h.write([0x30, (address & 0xff), (address>>8 & 0xff), 0, count])
        try:
            value = self.h.read(count+1, 100)
        except:
            print('MemRead: read error.')
        return(bytes(value[1:count+1]))
    def volts(self, gain, volt):
        """
        Function to convert volts (or current) to inteter value
        gain:
        UP_10_00V  = 0          # 0 - 10 V
        BP_10_00V   = 1         # +/- 10.00 V
        I_20_00mA = 2           # 0 - 20 mA
        """
        if gain == 0:
          if volt >= 10.:
            value = 0xffff
          elif volt <= 0.0:
            value = 0
          else:
            value = volt*65536./10.
        elif gain == 1:
          if volt >= 10.:
            value = 0xffff
          elif volt <- -10.:
            value = 0
          else:
            value = volt*32728./10 + 32768
        elif gain == 2:
          if volt >= 0.02: # 20 mA
            value = 0xffff
          elif volt <= 0.0:
            value = 0
          else:
            value = volt*249*65535./5.
        else:
          raise ValueError('volts: Unknwon gain level')
          return

        return int(value) & 0xffff
