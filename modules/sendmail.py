# -*- coding: utf-8 -*-
"""
Created on Tue Nov 27 10:56:03 2018

@author: Measurement
"""
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

def sendMail(self, channel, warning_voltage, filename, address_list):
    """function used for sending an email with measurement log to recipients"""
    try:
        print('sending email...')
        print('address list: {}'.format(address_list))
        fromaddr = "TheBatteries_Rzeszow_Measurement@outlook.com"
        toaddr = address_list
        msg = MIMEMultipart()
        msg['From'] = fromaddr
        msg['To'] = ", ".join(toaddr)
        #Subject: "[SAMPLE ID] - [MEASUREMENT TYPE] from [DATE] data "
        msg['Subject'] = ("{0} - "
                          "{1} from {2} data"
                          .format(self.sampleID[channel],
                                  self.meas_type[channel],
                                  self.date_channel[channel])
                          )
        if self.meas_type[channel] == 'charge':
        #Mail text: [SAMPLE ID] reached ...
            body = ("{} reached over {}"
                    "V during charging".format(self.sampleID[channel],
                                               warning_voltage))
        else:
            body = ("{} reached under {}"
                    "V during discharging".format(self.sampleID[channel],
                                                  warning_voltage))
        msg.attach(MIMEText(body, 'plain'))
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(filename, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment', filename=filename)
        msg.attach(part)
        server = smtplib.SMTP('smtp.office365.com', 587)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(fromaddr, 'tb2718281828')
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()
        print("Email with '{}' file has been sent".format(filename))
    except (smtplib.SMTPRecipientsRefused, smtplib.socket.gaierror) as error:
        print(error)
