# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 12:00:14 2018

@author: Roman
"""
import sys
import os
import time
import logging
import shutil
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import pyqtSlot
import pyqtgraph as pg
import modules.worker as worker
import modules.devicecontrol as devicecontrol

daq = devicecontrol.DAQ()
dac = devicecontrol.DAC()
# def logthread(caller):
    # """we check what thread is used while calling"""
    # print('%-25s: %s, %s,' % (caller, threading.current_thread().name,
    #                           threading.current_thread().ident))

class MyApp(QtWidgets.QMainWindow):
    workerObject = worker.WorkerObject(daq, dac)
    def __init__(self):
        super(MyApp, self).__init__()
        uic.loadUi(os.path.dirname(os.path.abspath(__file__))+os.sep+"dac.ui", self)
        self.initUI()
        self.check_platform()
        self.set_measurement_directory()
        
        self.moved = False

        self.plot_timer = QtCore.QTimer()
        self.plot_timer.timeout.connect(self.plot_update)

        self.threadPool = ['']*2
        self.channel_to_plot = None

        self.resistor = self._resistor()
        self.v_divider = self._v_divider()

    def initUI(self):
        """connecting every GUI elements there"""
        self.pushButton = (self.pushButton_START_ch0, self.pushButton_START_ch1,
                           self.pushButton_START_ch2, self.pushButton_START_ch3,
                           self.pushButton_START_ch4, self.pushButton_START_ch5,
                           self.pushButton_START_ch6, self.pushButton_START_ch7
                          )
        self.pushButton[0].clicked.connect(lambda: self.button_start_click_event(0))
        self.pushButton[1].clicked.connect(lambda: self.button_start_click_event(1))
        self.pushButton[2].clicked.connect(lambda: self.button_start_click_event(2))
        self.pushButton[3].clicked.connect(lambda: self.button_start_click_event(3))
        self.pushButton[4].clicked.connect(lambda: self.button_start_click_event(4))
        self.pushButton[5].clicked.connect(lambda: self.button_start_click_event(5))
        self.pushButton[6].clicked.connect(lambda: self.button_start_click_event(6))
        self.pushButton[7].clicked.connect(lambda: self.button_start_click_event(7))

        self.pushButton_emailList_add.clicked.connect(self.email_list_add_record)
        self.pushButton_emailList_remove.clicked.connect(self.email_list_remove_record)
        self.comboBox_channelList.activated.connect(self.select_plot_channel)
        self.pushButton_plot.released.connect(self.plot_init)

        self.voltage_label = (self.label_ch0_eng, self.label_ch1_eng,
                              self.label_ch2_eng, self.label_ch3_eng,
                              self.label_ch4_eng, self.label_ch5_eng,
                              self.label_ch6_eng, self.label_ch7_eng
                             )
        self.cycle_counter_label = (self.label_cycle_count_ch0, self.label_cycle_count_ch1,
                                    self.label_cycle_count_ch2, self.label_cycle_count_ch3,
                                    self.label_cycle_count_ch4, self.label_cycle_count_ch5,
                                    self.label_cycle_count_ch6, self.label_cycle_count_ch7
                                   )

        self.sampleID_box = (self.lineEdit_sampleID_ch0, self.lineEdit_sampleID_ch1,
                             self.lineEdit_sampleID_ch2, self.lineEdit_sampleID_ch3,
                             self.lineEdit_sampleID_ch4, self.lineEdit_sampleID_ch5,
                             self.lineEdit_sampleID_ch6, self.lineEdit_sampleID_ch7
                            )

        self.lineEdit_charge_warning.editingFinished.connect(self.update_value)
        self.lineEdit_discharge_warning.editingFinished.connect(self.update_value)
        self.lE_delay.editingFinished.connect(self.update_value)
        self.checkBox_send_email.stateChanged.connect(self.update_value)
        self.lE_relax_time.editingFinished.connect(self.update_value)
        self.workerObject.updateLabel.connect(self.update_voltage_label)
        self.workerObject.updatePushButton.connect(self.update_button_text)
        self.workerObject.sendToServer.connect(self.upload_files_to_server)
        self.workerObject.update_counter_label.connect(self.update_counter_labels)
        self.workerObject.set_AOut.connect(self.set_analog_out)
        self.email_objects_show_and_hide()
        self.show()

    def _resistor(self):
        #           #channel #1st value #2nd value #3rd value
        resistor =  {0: {0: 38.81,  1: 19.438, 2: 7.848},
                     1: {0: 38.79,  1: 19.288, 2: 7.865},
                     2: {0: 38.74,  1: 19.385, 2: 7.866},
                     3: {0: 38.68,  1: 19.352, 2: 7.863},
                     4: {0: 38.715, 1: 19.374, 2: 7.884},
                     5: {0: 38.456, 1: 19.326, 2: 7.854},
                     6: {0: 40.8,   1: 19.877, 2: 7.868},
                     7: {0: 36.3,   1: 18.731, 2: 7.779}
                    }
        return resistor
    def _v_divider(self):
        """ Values for voltage divider for every channel """
        v_divider = {0: 0.163,
                     1: 0.163,
                     2: 0.166625,
                     3: 0.16666,
                     4: 0.160613,
                     5: 0.163838,
                     6: 0.1654147,
                     7: 0.163912}
        return v_divider
    def check_platform(self):
        import platform
        if platform.uname()[0] == 'Windows':
            self.operating_system = 'Windows'
            
        else:# raspberry
            self.operating_system = 'Pi'
        self.workerObject.operating_system = self.operating_system
    def set_measurement_directory(self):
        """ Set directory to measurement folder """
        if self.operating_system == 'Windows':
            self.directory = "C:\\Users\\Measurement\\Desktop\\Measurements\\"
        else:
            self.directory = "/home/pi/Documents/Measurements/"
        self.workerObject.directory = self.directory
    def update_value(self):
        """Update values for finish charge, finish discharge and state to send email or not"""
        sender = self.sender()
        if sender.objectName() == 'lineEdit_charge_warning':
            charge_voltage = self.lineEdit_charge_warning.text().replace(',','.')
            self.lineEdit_charge_warning.setText(charge_voltage)
            self.workerObject.warning_charge = float(charge_voltage)
            print(('Charge warning value: '
                   '{} V'.format(charge_voltage)
                  ))
        elif sender.objectName() == 'lineEdit_discharge_warning':
            discharge_voltage = self.lineEdit_discharge_warning.text().replace(',','.')
            self.lineEdit_discharge_warning.setText(discharge_voltage)
            self.workerObject.warning_discharge = float(discharge_voltage)
            print(('Discharge warning value: '
                   '{} V'.format(discharge_voltage)
                  ))
        elif sender.objectName() == 'lE_delay':
            self.workerObject.delay = float(sender.text())
            self.delay = float(sender.text())
            print("Measurement delay set to {} seconds".format(sender.text()))
        elif sender.objectName() == 'lE_relax_time':
            self.workerObject.relax_time = int(sender.text())
            print("Relaxation time during cycling set to {} seconds".format(sender.text()))
        elif sender.objectName() == 'checkBox_send_email':
            self.workerObject.send_email = self.checkBox_send_email.isChecked()
            self.email_objects_show_and_hide()
    def button_start_click_event(self, channel):
        """ when START/STOP button is pressed, change button's text, i.e
            if we press START, button change text to STOP and reverse.
            If button is pressed to START the measurement, create popup window
            which is required to initialize measurements.
            If button is pressed to STOP the measurement, we close DIO channel
            so current flow for this channel is closed.
        """
        if self.workerObject.channel_status[channel] == 'OFF':
            self.popup_init_measurements(channel)
        else:
            self.workerObject.channel_status[channel] = 'OFF'
            # set digital output in specified channel to 0 V when STOP button is pressed:
            # daq.DOut(state)
            # daq.set_digital_output(channel, 1)
            # print("DAC voltage set to 0")
            # set DAC D/A output to 0:
            dac.AOut(channel, 0)
            print('Measurement stopped. Channel: {}'.format(channel))
            self.update_button_text(channel, 'START')
    def predict_operation_type(self, channel):
        """ Iterate over all files in sampleID folder, get a number of 
            last operation, fill lineEdit box with next value, so operator doesn't have to
            look for next operation manually
        """
        ID = str(self.sampleID_box[channel].text())
        fulldir = self.directory + ID
        if os.path.isdir(fulldir) and any(os.path.isfile(os.path.join(fulldir,i)) for i in os.listdir(fulldir)):
            number = []
            op_type = []
            for file in os.listdir(fulldir):
                try:
                    split_file = file.split('_')
                    number.append(int(split_file[-1][:-4]))
                    op_type.append(split_file[-2])
                except (ValueError, IndexError):
                    continue
            if number and op_type:
                number, op_type = zip(*sorted(zip(number, op_type)))
                next_operation_number = str(max(number)+1)
                if op_type[-1] == 'charge': #check last operation type
                    next_operation_type = 'discharge'
                else:
                    next_operation_type = 'charge'
            else:
                next_operation_number = '1'
                next_operation_type = None
        else:
            next_operation_number = '1'
            next_operation_type = None
        return next_operation_number, next_operation_type
    def popup_init_measurements(self, channel):
        """ create popup window after START button is pressed
            create slot for push button inside popup window,
            show the window
        """
        next_operation_number, next_operation_type = self.predict_operation_type(channel)
        resistor_values = self.resistor[channel]

        self.p = Popup()

        if next_operation_type == 'discharge':
            self.p.radioButton_discharge.setChecked(True)
        else:
            self.p.radioButton_charge.setChecked(True)
        self.p.lineEdit_operation_number.setText(next_operation_number)

        self.p.rB_r_1.setText(str(resistor_values[0]))
        self.p.rB_r_2.setText(str(resistor_values[1]))
        self.p.rB_r_3.setText(str(resistor_values[2]))

        self.p.pushButton_OK.clicked.connect(lambda: self.init_measurement(channel))
        self.p.show()
    
    def init_measurement(self, channel):
        """Start measurements - gather all informations about sample and the process, such as:
        sample ID, date, measurement type, sample number, charge/discharge current.
        Set filename, set 5V on DAQ digital output, append Excel log file, start worker thread
        """
        self.update_button_text(channel, 'STOP') #change START/STOP button text to STOP
        self.p.close()
        # create worker therad:
        self.moveThread()
        # get all informations about measurement:
        cycle = self.p.cB_cycle.isChecked()
        if cycle:
            # print('### CYCLE is ON, gather data: ')
            (sampleID, meas_type, current, operation_number, cycle_number) = self.get_sample_data(channel, cycle)
            # print("### DATA GATHERED, SEND TO WORKER OBJECT")
            self.workerObject.update_parameters_cycle.emit(channel, sampleID, meas_type,
                                                         current, operation_number, cycle, cycle_number)
            # print("### set cycle counter label")
            self.update_counter_labels(channel, 1, cycle_number*2, meas_type)
            # self.cycle_counter_label[channel].setText('{} / {} {}'.format(1, cycle_number*2, meas_type))
            # print("### cycle counter label set")
        else:
            # print("NO CYCLE")
            (sampleID, meas_type, current, operation_number) = self.get_sample_data(channel, cycle)
            # print("got sample data")
            self.workerObject.update_parameters.emit(channel, sampleID, meas_type,
                                                     current, operation_number, cycle)
            # print("update update_parameters signal emit")
            self.update_counter_labels(channel, 1, 1, meas_type)
            # self.cycle_counter_label[channel].setText('1 / 1')
        self.workerObject.init_measurements.emit(channel, False)

        ##################################################
        # set DAQ DIO channel to 5 V:                   ##
        # daq.set_digital_output(channel, 0)              ##
        ##################################################
        print('{} started. Channel: {}'.format(meas_type, channel))
        self.workerObject.start.emit()

    def get_sample_data(self, channel, *cycle):
        """all informations about measurements are gathered there:
        - sampleID
        - date: date of measurement, format: YYYY-MM-DD_HH-MM-SS
        - filnename
        - charge/discharge current: it's provided manually, because we can only measure analog input
        - operation number 
        """ 
        operation_number = int(self.p.lineEdit_operation_number.text())
        sampleID = str(self.sampleID_box[channel].text())
        current = float(self.p.lineEdit_current.text().replace(',','.')) #charge/discharge current
        if self.p.radioButton_charge.isChecked():
            meas_type = 'charge'
        else:
            meas_type = 'discharge'
        if cycle[0]:
            cycle_number = int(self.p.lE_number_of_cycles.text())
            print(('Cycle operation mode:\nchannel: {},\noperation_number: {},\nsampleID: {},\ncurrent: {},\n'
                   'measurement_type: {},\nhow many cycles: {}').format(
                   channel, operation_number, sampleID, current, meas_type, cycle_number))
            return sampleID, meas_type, current, operation_number, cycle_number
        else:
            print(('Single operation mode:\nchannel: {},\noperation_number: {},\nsampleID: {},\ncurrent: {},\n'
                   'measurement_type: {}').format(
                   channel, operation_number, sampleID, current, meas_type))
            return sampleID, meas_type, current, operation_number

    def set_analog_out(self, channel, current):
        """Set D/A output channel on DAC with specified voltage

        Args:
        channel: int 
            channel number on DAC board
        current: float
            desired current we want to obtain on charge/discharge operation
        """
        #select the resistance value on specified channel:
        buttons = [self.p.rB_r_1, self.p.rB_r_2, self.p.rB_r_3]
        checked_button = [b.isChecked() for b in buttons].index(True)
        #measure the voltage we need to get current we want to apply
        Va = (current * self.resistor[channel][checked_button])/1000
        #set voltage on DAQ, which is Va multipied by the value of voltage divider
        Vdac = Va / self.v_divider[channel]
        # set DAC D/A output:
        dac.AOut(channel, Vdac)
        

    def moveThread(self):
        """Here we create new QThread to avoid freezing GUI"""
        if not self.moved:
            self.my_thread = QtCore.QThread()
            self.my_thread.start()
            self.workerObject.moveToThread(self.my_thread)
            self.moved = True
    def select_plot_channel(self):
        """select what channel should be plotted"""
        self.channel_to_plot = self.comboBox_channelList.currentIndex()
    def plot_init(self):
        """create plot window, trigger QTimer which refreshes the plot
        Note: needs to check in raspberry how plots are working after 20+ hours of data acquisition
        CPU usage after that long time might be pretty high
        """
        self.win = pg.GraphicsWindow("plot")
        self.win.resize(1000, 600)
        self.plt = self.win.addPlot(row=1, col=0, title="Plot",
                                    labels={'left': 'Voltage [V]',
                                            'bottom': 'Capacity [mAh]'
                                            }
                                    )
        self.curve = self.plt.plot(pen='b')

        self.label_cords = pg.LabelItem(justify="right")
        self.win.addItem(self.label_cords)
        self.proxy = pg.SignalProxy(self.plt.scene().sigMouseMoved,
                                    rateLimit=60, slot=self.plot_mouse_moved)

        self.plot_timer.start(2000)
    def plot_mouse_moved(self, evt):
        """create label with mouse position on the plot window"""
        self.mousePoint = self.plt.vb.mapSceneToView(evt[0])
        self.label_cords.setText(("<span style='font-size: 8pt; color: white'> "
                                  "x = {:0.4f}, "
                                  "<span style='color: white'> "
                                  "y = {:0.4f} </span>\n"
                                  .format(self.mousePoint.x(), self.mousePoint.y())
                                 ))
    def plot_update(self):
        try:
            self.curve.setData(self.workerObject.data_dictionary[self.channel_to_plot]['Capacity'],
                               self.workerObject.data_dictionary[self.channel_to_plot]['Voltage'],
                               pen=pg.mkPen('b', width=3)
                               )
        except Exception as e:
            logging.error('plot window closed, because empty channel was chosen')
            self.plot_timer.stop()
            self.win.close()
            print("DataFrame for channel {} is empty".format(self.channel_to_plot))
        if not self.win.isVisible():
            print('Plot window closed')
            self.plot_timer.stop()
    def email_list_add_record(self):
        """ add a record to email list"""
        self.listWidget_emailList.addItem(self.lineEdit_emailList_address.text())
        self.email_address_list_update()
    def email_list_remove_record(self):
        """ remove a record from email list"""
        listItems = self.listWidget_emailList.selectedItems()
        if not listItems: return
        for item in listItems:
            self.listWidget_emailList.takeItem(self.listWidget_emailList.row(item))
        self.email_address_list_update()
    def email_address_list_update(self):
        """refresh GUI box with email addresses"""
        address_list = []
        for index in range(self.listWidget_emailList.count()):
            address_list.append(str(self.listWidget_emailList.item(index).text()))
        self.workerObject.addressList = address_list
    def email_objects_show_and_hide(self):
        """if 'send email' checkbox isn't checked, hide related GUI elements.
            Show related GUI elements when checkbox is checked"""
        if not self.checkBox_send_email.isChecked():
            self.label_13.hide()
            self.listWidget_emailList.hide()
            self.pushButton_emailList_remove.hide()
            self.pushButton_emailList_add.hide()
            self.lineEdit_emailList_address.hide()
            self.label_14.hide()
        else:
            self.label_13.show()
            self.listWidget_emailList.show()
            self.pushButton_emailList_remove.show()
            self.pushButton_emailList_add.show()
            self.lineEdit_emailList_address.show()
            self.label_14.show()
    def update_voltage_label(self, channel_num, read_value):
        """update GUI label, with last DAQ voltage read"""
        self.voltage_label[channel_num].setText('{:5f}'.format(read_value))
    def update_counter_labels(self, channel, current_operation, all_operations, meas_type):
        #
        self.cycle_counter_label[channel].setText('{} / {}  {}'.format(current_operation, all_operations, meas_type))
    def update_button_text(self, channel, text, *enabled):
        """Change text of START/STOP push button"""
        self.pushButton[channel].setText(text)
        if enabled:
            if self.pushButton[channel].text() == 'START':
                self.pushButton[channel].setEnabled(enabled[0])
            else:
                self.pushButton[channel].setEnabled(enabled[0])
    
    def upload_files_to_server(self, filename):
        """Copy log file and override excel log file on local web hard drive"""
        try:
            filename = str(filename)

            if self.operating_system == 'Windows':
                server_address = '//192.168.0.250//roman//Measurements'

                excelFile_path = ("C:\\Users\\Measurement\\Desktop\\Measurements\\"
                                  "Measurements_LogFile.xlsx"
                                 )

                server_folder = (server_address
                                 + os.sep
                                 + os.path.basename(os.path.dirname(filename))
                                 + os.sep
                                )

                server_file_path = (server_folder
                                    + os.path.basename(os.path.normpath(filename))
                                   )
            else:
                server_address = '/home/pi/myNAS/Measurements'
                excelFile_path = "/home/pi/Documents/Measurements/Measurements_LogFile.xlsx"
                server_folder = server_address+os.sep+os.path.basename(os.path.dirname(filename))+os.sep
                server_file_path = server_folder+os.path.basename(os.path.normpath(filename))

            if not os.path.exists(server_folder):
                os.makedirs(server_folder)
            print(('Creating copy of measurement log on the server, '
                   'directory: {}'.format(server_file_path)))
            shutil.copy2(filename, server_file_path)
            shutil.copy2(excelFile_path, server_address)
            print("Files copied")
        except Exception as e:
            print(e)
    def closeEvent(self, event):
        """ Override default PyQt's closeEvent, so there's prompt on exit now """
        quit_msg = "Are you sure you want to exit the program?"
        reply = QtGui.QMessageBox.question(self, 'Message', 
                         quit_msg, QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            for channel in range(8):
                daq.DOut(channel, 1)
                dac.AOut(channel, 0)
            daq.udev.close()
            dac.h.close()
            event.accept()
        else:
            event.ignore()
class Popup(QtWidgets.QWidget):
    """QWidget class for popup window when START button is pressed
    created for later development but it's already working"""
    def __init__(self):                                                        #
        super(Popup, self).__init__()                                        #
        uic.loadUi(os.path.dirname(os.path.abspath(__file__)) + os.sep+"popup.ui", self)
        # self.hide()
        self.show_cycle_elements()
        self.cB_cycle.stateChanged.connect(self.show_cycle_elements)
    def show_cycle_elements(self):
        if self.cB_cycle.isChecked():
            self.label_5.show()
            self.lE_number_of_cycles.show()
        else:
            self.label_5.hide()
            self.lE_number_of_cycles.hide()

#########################################################################################################
logging.basicConfig(filename='crashlog.log', format='%(asctime)s %(message)s', level=logging.WARNING)   #                                                                      #
def handle_exception(exc_type, exc_value, exc_traceback):                                               #
    if issubclass(exc_type, KeyboardInterrupt):                                                         #
        sys.__excepthook__(exc_type, exc_value, exc_traceback)                                          #
        return                                                                                          #
    print('error:\n{0}\n{1}\n{2}'.format(exc_type, exc_value, exc_traceback))                           # 
    logging.critical("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))               #
sys.excepthook = handle_exception                                                                       #
#########################################################################################################
def main():
    print('Charge/Discharge Board control application, Hello !')
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle("Fusion")
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
if __name__ == "__main__":
        main()
    
